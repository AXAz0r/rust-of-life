# Rust of Life

[Conway's Game of Life](https://en.wikipedia.org/wiki/Conway's_Game_of_Life) written in Rust.

It's horribly unoptimized. Especially if you try running it with debugging symbols.

## Look

It looks something like this...

![life-screen](https://i.imgur.com/FFRasD0.png)


## Download

You can find the GitLab CI build artifacts [here](https://gitlab.com/AXAz0r/rust-of-life/-/jobs/artifacts/master/download?job=publish-gitlab).
Note that these are build for Unix-like systems.
