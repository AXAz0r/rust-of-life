use crate::board::Board;
use crate::params::Params;
use crate::state::State;

#[derive(Clone)]
pub struct Cell {
    pub x: i64,
    pub y: i64,
    pub state: State,
    pub params: Params,
}

impl Cell {
    pub fn new(params: Params, x: i64, y: i64) -> Self {
        Self {
            x,
            y,
            state: State::Dead,
            params,
        }
    }

    pub fn toggle(&mut self) {
        if self.state == State::Alive {
            self.state = State::Dead;
        } else {
            self.state = State::Alive;
        }
    }

    pub fn draw(&self) -> String {
        format!(
            "{}",
            match self.state {
                State::Dead => &self.params.dead_char,
                State::Alive => &self.params.alive_char,
            }
        )
    }

    pub fn count_neighbours(&self, board: Board, state: State) -> i64 {
        let mut count = 0;
        let modifiers = vec![-1, 0, 1];
        for mod_x in modifiers.clone() {
            for mod_y in modifiers.clone() {
                if !(mod_x == 0 && mod_y == 0) {
                    let tgt_x = self.x + mod_x;
                    let tgt_y = self.y + mod_y;
                    let target = board.get_cell(tgt_x, tgt_y);
                    match target {
                        Some(target) => {
                            if target.state == state {
                                count += 1;
                            }
                        }
                        None => {}
                    }
                }
            }
        }
        count
    }
}
