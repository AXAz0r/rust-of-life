#[derive(Debug, Clone)]
pub enum State {
    Dead,
    Alive,
}

impl PartialEq for State {
    fn eq(&self, other: &Self) -> bool {
        self.as_int() == other.as_int()
    }
}

impl State {
    pub fn as_int(&self) -> i64 {
        match self {
            State::Dead => 0,
            State::Alive => 1,
        }
    }
}
