use crate::cell::Cell;
use crate::params::Params;
use crate::state::State;
use rand::Rng;

#[derive(Clone)]
pub struct Board {
    pub rows: Vec<Vec<Cell>>,
    pub params: Params,
}

impl Board {
    pub fn new(params: Params) -> Self {
        let mut rows = Vec::<Vec<Cell>>::new();
        for rix in 0..params.height {
            let mut row = Vec::<Cell>::new();
            for cix in 0..params.width {
                row.push(Cell::new(params.clone(), cix as i64, rix as i64));
            }
            rows.push(row);
        }
        Self { params, rows }
    }

    pub fn max_width(&self) -> i64 {
        if self.rows.is_empty() {
            0i64
        } else {
            self.rows[0].len() as i64
        }
    }

    pub fn max_height(&self) -> i64 {
        self.rows.len() as i64
    }

    pub fn get_cell(&self, x: i64, y: i64) -> Option<&Cell> {
        let under_max = x < self.max_width() && y < self.max_height();
        let over_min = x > 0 && y > 0;
        if over_min && under_max {
            match self.rows.get(y as usize) {
                Some(row) => match row.get(x as usize) {
                    Some(cell) => Some(cell),
                    None => None,
                },
                None => None,
            }
        } else {
            None
        }
    }

    pub fn set_cell(&mut self, x: i64, y: i64, new: Cell) {
        self.rows[y as usize][x as usize] = new;
    }

    pub fn init(&mut self) {
        for coord in self.params.clone().init_cells {
            let mut cell = self.rows[coord.y as usize][coord.x as usize].clone();
            cell.toggle();
            self.set_cell(coord.x as i64, coord.y as i64, cell);
        }
    }

    pub fn randomize(&mut self) {
        let mut rng = rand::thread_rng();
        for _ in 0..self.params.random_cells {
            let rand_x = if self.max_width() != 0 {
                rng.gen_range(0, self.max_width() - 1)
            } else {
                0
            };
            let rand_y = if self.max_height() != 0 {
                rng.gen_range(0, self.max_height() - 1)
            } else {
                0
            };
            let mut cell = self.rows[rand_y as usize][rand_x as usize].clone();
            cell.toggle();
            self.set_cell(rand_x, rand_y, cell);
        }
    }

    pub fn calculate(&mut self) {
        let mut changes = Vec::<Cell>::new();
        for row in self.rows.clone() {
            for cell in row {
                let neighbours = cell.count_neighbours(self.clone(), State::Alive);
                match cell.state {
                    State::Alive => {
                        if neighbours < 2 || neighbours > 3 {
                            let mut new = cell.clone();
                            new.toggle();
                            changes.push(new);
                        }
                    }
                    State::Dead => {
                        if neighbours == 3 {
                            let mut new = cell.clone();
                            new.toggle();
                            changes.push(new);
                        }
                    }
                }
            }
        }
        for cell in changes {
            self.set_cell(cell.x, cell.y, cell);
        }
    }

    pub fn draw(&self) -> String {
        let mut rows_drawn = Vec::<String>::new();
        for row in &self.rows {
            let mut cells_drawn = Vec::<String>::new();
            for cell in row {
                cells_drawn.push(cell.draw());
            }
            rows_drawn.push(format!(
                "{}{}{}",
                self.params.separator_char,
                cells_drawn.join(&self.params.separator_char),
                self.params.separator_char
            ));
        }
        let repeat = self.params.separator_char.len() as i64;
        let border = format!(
            "{}",
            self.params
                .border_char
                .repeat(((self.max_width() + (self.max_width() * repeat)) + repeat) as usize)
        );
        format!("{}\n{}\n{}", &border, rows_drawn.join("\n"), border)
    }
}
