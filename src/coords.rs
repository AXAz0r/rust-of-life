#[derive(Clone)]
pub struct Coordinates {
    pub x: u64,
    pub y: u64,
}

impl Coordinates {
    pub fn new(x: u64, y: u64) -> Self {
        Self { x, y }
    }
}
