use crate::coords::Coordinates;
use crate::error::LifeError;
use clap::{
    app_from_crate, crate_authors, crate_description, crate_name, crate_version, Arg, ArgMatches,
};

const DEFAULT_WIDTH: &'static str = "38";
const DEFAULT_HEIGHT: &'static str = "16";
const DEFAULT_YEARS: &'static str = "1000";
const DEFAULT_RANDOM_CELLS: &'static str = "150";
const DEFAULT_DELAY: &'static str = "125";
const DEFAULT_DEAD_CHAR: &'static str = " ";
const DEFAULT_ALIVE_CHAR: &'static str = "■";
const DEFAULT_BORDER_CHAR: &'static str = "-";
const DEFAULT_SEPARATOR_CHAR: &'static str = "|";

#[derive(Clone)]
pub struct Params {
    pub width: u64,
    pub height: u64,
    pub years: u64,
    pub random_cells: u64,
    pub init_cells: Vec<Coordinates>,
    pub delay: u64,
    pub dead_char: String,
    pub alive_char: String,
    pub border_char: String,
    pub separator_char: String,
    pub no_clear: bool,
}

impl Params {
    pub fn new() -> Result<Self, LifeError> {
        let matches = app_from_crate!()
            .arg(
                Arg::with_name("width")
                    .short("w")
                    .long("width")
                    .value_name("width")
                    .help("Sets the width of the grid")
                    .default_value(DEFAULT_WIDTH)
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("height")
                    .short("h")
                    .long("height")
                    .value_name("height")
                    .help("Sets the height of the grid")
                    .default_value(DEFAULT_HEIGHT)
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("years")
                    .short("y")
                    .long("years")
                    .value_name("years")
                    .help("Sets the maximum number of years/generations")
                    .default_value(DEFAULT_YEARS)
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("random-cells")
                    .long("random-cells")
                    .value_name("random-cells")
                    .help("Sets the number of random starting cells")
                    .default_value(DEFAULT_RANDOM_CELLS)
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("init-cells")
                    .long("init-cells")
                    .value_name("init-cells")
                    .help("Sets the coordinates of the initial alive cells.\nThis will override the random-cells argument.\nUse the format \"x:y; x;y; etc.\"")
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("delay")
                    .long("delay")
                    .value_name("delay")
                    .help("Sets the minimum delay between generations, in milliseconds")
                    .default_value(DEFAULT_DELAY)
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("dead-char")
                    .long("dead-char")
                    .value_name("dead-char")
                    .help("Sets the character representing dead cells")
                    .default_value(DEFAULT_DEAD_CHAR)
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("alive-char")
                    .long("alive-char")
                    .value_name("alive-char")
                    .help("Sets the character representing alive cells")
                    .default_value(DEFAULT_ALIVE_CHAR)
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("border-char")
                    .long("border-char")
                    .value_name("border-char")
                    .help("Sets the character representing the top and bottom borders of the grid")
                    .default_value(DEFAULT_BORDER_CHAR)
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("separator-char")
                    .long("separator-char")
                    .value_name("separator-char")
                    .help("Sets the character representing the separator between cells")
                    .default_value(DEFAULT_SEPARATOR_CHAR)
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("no-clear")
                    .long("no-clear")
                    .help("Prevents the console from being cleared after each generation")
            )
            .get_matches();
        let width = matches
            .value_of("width")
            .unwrap_or(&DEFAULT_WIDTH.to_string())
            .parse::<u64>()?;
        let height = matches
            .value_of("height")
            .unwrap_or(&DEFAULT_HEIGHT.to_string())
            .parse::<u64>()?;
        let years = matches
            .value_of("years")
            .unwrap_or(&DEFAULT_YEARS.to_string())
            .parse::<u64>()?;
        let random_cells = matches
            .value_of("random-cells")
            .unwrap_or(&DEFAULT_RANDOM_CELLS.to_string())
            .parse::<u64>()?;
        let init_cells = Self::get_init_cells(&matches)?;
        let delay = matches
            .value_of("delay")
            .unwrap_or(&DEFAULT_DELAY.to_string())
            .parse::<u64>()?;
        let dead_char = matches
            .value_of("dead-char")
            .unwrap_or(DEFAULT_DEAD_CHAR)
            .to_owned();
        let alive_char = matches
            .value_of("alive-char")
            .unwrap_or(DEFAULT_ALIVE_CHAR)
            .to_owned();
        let border_char = matches
            .value_of("border-char")
            .unwrap_or(DEFAULT_BORDER_CHAR)
            .to_owned();
        let separator_char = matches
            .value_of("separator-char")
            .unwrap_or(DEFAULT_SEPARATOR_CHAR)
            .to_owned();
        let no_clear = matches.is_present("no-clear");
        Ok(Self {
            width,
            height,
            years,
            random_cells,
            init_cells,
            delay,
            dead_char,
            alive_char,
            border_char,
            separator_char,
            no_clear,
        })
    }

    fn get_init_cells(matches: &ArgMatches) -> Result<Vec<Coordinates>, LifeError> {
        let mut cells = Vec::<Coordinates>::new();
        let init_cells_str = matches.value_of("init-cells").unwrap_or("");
        if init_cells_str != "" {
            let coord_pieces: Vec<&str> = init_cells_str.trim().split(';').collect();
            for coord_piece in coord_pieces {
                let xy_piece: Vec<&str> = coord_piece.split(':').collect();
                let x = xy_piece[0].trim().parse::<u64>()?;
                let y = xy_piece[1].trim().parse::<u64>()?;
                let coords = Coordinates::new(x, y);
                cells.push(coords);
            }
        }
        Ok(cells)
    }
}
