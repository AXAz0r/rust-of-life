use std::num::ParseIntError;

#[derive(Debug)]
pub enum LifeError {
    ParseIntError(ParseIntError),
}

impl From<ParseIntError> for LifeError {
    fn from(err: ParseIntError) -> Self {
        LifeError::ParseIntError(err)
    }
}
