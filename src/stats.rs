use crate::board::Board;
use crate::state::State;

#[derive(Debug, Default, Clone)]
pub struct Stats {
    pub year: u64,
    pub dead: u64,
    pub alive: u64,
    pub births: u64,
    pub deaths: u64,
}

impl Stats {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn count(&mut self, board: Board) {
        let mut dead = 0u64;
        let mut alive = 0u64;
        for row in &board.rows {
            for cell in row {
                match cell.state {
                    State::Dead => dead += 1,
                    State::Alive => alive += 1,
                }
            }
        }
        self.dead = dead;
        self.alive = alive;
    }

    pub fn add_year(&mut self) {
        self.year += 1;
    }

    pub fn add_change(&mut self, diff: Difference) {
        self.births += diff.born;
        self.deaths += diff.died;
    }

    pub fn draw(&self) -> String {
        format!(
            "Year: {}\nAlive: {} | Dead: {}\nBirths: {} | Deaths: {}",
            &self.year, &self.alive, &self.dead, &self.births, &self.deaths
        )
    }
}

#[derive(Debug, Clone)]
pub struct Difference {
    pub born: u64,
    pub died: u64,
}

impl Difference {
    pub fn compare(before: Board, after: Board) -> Self {
        let mut born = 0u64;
        let mut died = 0u64;
        for before_row in before.rows.clone() {
            for before_cell in before_row {
                let after_cell = after.rows[before_cell.y as usize][before_cell.x as usize].clone();
                if before_cell.state != after_cell.state {
                    match before_cell.state {
                        State::Dead => born += 1,
                        State::Alive => died += 1,
                    }
                }
            }
        }
        Self { born, died }
    }

    pub fn draw(&self) -> String {
        format!("Born: {} | Died: {}", &self.born, &self.died)
    }

    pub fn no_change(&self) -> bool {
        self.born == 0 && self.died == 0
    }
}
