use crate::board::Board;
use crate::error::LifeError;
use crate::params::Params;
use crate::stats::{Difference, Stats};
use crate::util::Utility;
use std::time::Duration;

mod board;
mod cell;
mod coords;
mod error;
mod params;
mod state;
mod stats;
mod util;

fn main() -> Result<(), LifeError> {
    let params = Params::new()?;
    let mut dead = false;
    let mut stats = Stats::new();
    let mut board = Board::new(params.clone());
    while stats.year < params.years && !dead {
        Utility::clear();
        let old_board = board.clone();
        if stats.year == 0 {
            if params.init_cells.is_empty() {
                board.randomize();
            } else {
                board.init();
            }
        } else {
            board.calculate();
        }
        let difference = Difference::compare(old_board, board.clone());
        if difference.no_change() {
            dead = true;
        }
        stats.count(board.clone());
        stats.add_change(difference.clone());
        stats.add_year();
        println!("{}", stats.draw());
        println!("{}", board.draw());
        println!("{}", difference.draw());
        std::thread::sleep(Duration::from_millis(params.delay));
    }
    if dead {
        println!("No difference detected on last iteration, stopping.");
    }
    Ok(())
}
